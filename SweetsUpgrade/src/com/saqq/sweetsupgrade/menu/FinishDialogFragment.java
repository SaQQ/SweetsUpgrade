package com.saqq.sweetsupgrade.menu;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


    
 
 @SuppressLint("NewApi")
public class FinishDialogFragment extends DialogFragment {
    	
    	int score = 0;
    	
    	public static FinishDialogFragment newInstance(int score) {
	        FinishDialogFragment f = new FinishDialogFragment();
       
	        Bundle args = new Bundle();
	        args.putInt("score", score);
	        f.setArguments(args);
	        return f;
	    }
    	
	 public Dialog onCreateDialog(Bundle savedInstanceState) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        score = getArguments().getInt("score");
	        builder.setTitle("You lost!")
	        		.setMessage("Your score: "+ score)
	               .setPositiveButton("Back to menu", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	  getActivity().finish();
	                   }
	               });
	               
	        
	        return builder.create();
	    }
}

