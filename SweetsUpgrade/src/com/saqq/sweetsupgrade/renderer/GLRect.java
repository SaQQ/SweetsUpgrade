package com.saqq.sweetsupgrade.renderer;

import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindBuffer;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glBufferData;
import static android.opengl.GLES20.glDisableVertexAttribArray;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glVertexAttribPointer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.saqq.sweetsupgrade.GameObject;

public class GLRect implements GameObject {
	
	private final FloatBuffer vertexBuffer;
	private final ByteBuffer indexBuffer;

	private int buffers[] = new int[2]; // 0 = array buffer, 1 = index buffer
	
	private int textureHandle;
	
	protected float x;
	protected float y;
	
	protected float alpha;
	
	public GLRect(float w, float h) {
		
		final float[] vertices = new float[] {
		//		 X    Y      Z     R     G     B     A     Nx    Ny    Nz   TexS  TexT
				0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
				   w, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
				   w,    h, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
				0.0f,    h, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		};
		
		vertexBuffer = ByteBuffer.allocateDirect(vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.put(vertices).position(0);
		
		final byte[]  indices = new byte[] {
			0, 1, 2,
			0, 2, 3,
		};
		
		indexBuffer = ByteBuffer.allocateDirect(indices.length).order(ByteOrder.nativeOrder());
		indexBuffer.put(indices).position(0);
		
		glGenBuffers(2, buffers, 0);
		
		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
		glBufferData(GL_ARRAY_BUFFER, vertices.length * 4, vertexBuffer, GL_STATIC_DRAW);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.length, indexBuffer, GL_STATIC_DRAW);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		this.x = 0;
		this.y = 0;
				
		alpha = 1.0f;
	}
	
	public GLRect(float[] vertices) {

		//**********TEMPLATE**********//

		/*final float[] vertices = new float[] {
		//		 X    Y      Z     R     G     B     A     Nx    Ny    Nz   TexS  TexT
				0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
				   w, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
				   w,    h, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
				0.0f,    h, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		};*/
		
		vertexBuffer = ByteBuffer.allocateDirect(vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.put(vertices).position(0);
		
		final byte[]  indices = new byte[] {
			0, 1, 2,
			0, 2, 3,
		};
		
		indexBuffer = ByteBuffer.allocateDirect(indices.length).order(ByteOrder.nativeOrder());
		indexBuffer.put(indices).position(0);
		
		glGenBuffers(2, buffers, 0);
		
		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
		glBufferData(GL_ARRAY_BUFFER, vertices.length * 4, vertexBuffer, GL_STATIC_DRAW);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.length, indexBuffer, GL_STATIC_DRAW);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		this.x = 0;
		this.y = 0;
		
		setTexture(TextureUtils.getTexture("tex"));
	}
	
	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();
		
		r.pushMatrix();
		
		r.translate(x, y, 0);
		
		modifyMatrix(r);
		
		r.applyMatrix();
		
		r.setAlpha(alpha);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureHandle);
		
		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);

		glVertexAttribPointer(POSITION_HANDLE, 3, GL_FLOAT, false, 48, 0);
		glEnableVertexAttribArray(POSITION_HANDLE);
		
		glVertexAttribPointer(COLOR_HANDLE, 4, GL_FLOAT, false, 48, 12);
		glEnableVertexAttribArray(COLOR_HANDLE);
		
		glVertexAttribPointer(NORMAL_HANDLE, 3, GL_FLOAT, false, 48, 28);
		glEnableVertexAttribArray(NORMAL_HANDLE);
		
		glVertexAttribPointer(TEXTURE_HANDLE, 2, GL_FLOAT, false, 48, 40);
		glEnableVertexAttribArray(TEXTURE_HANDLE);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
		
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		glDisableVertexAttribArray(POSITION_HANDLE);
		glDisableVertexAttribArray(COLOR_HANDLE);
		glDisableVertexAttribArray(NORMAL_HANDLE);
		glDisableVertexAttribArray(TEXTURE_HANDLE);
		
		r.popMatrix();
	}
	
	public float getX() {
		return this.x;
	}
	
	public float getY() {
		return this.y;
	}
	
	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	protected void modifyMatrix(GLRenderer r) {
		
	}
	
	public void setPosition(final float x, final float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setTexture(int texID) {
		this.textureHandle = texID;
	}
	
	@Override
	public void update(int t) {
		
	}

}
