package com.saqq.sweetsupgrade.renderer;

import static android.opengl.GLES20.GL_NEAREST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glTexParameteri;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.saqq.sweetsupgrade.R;

public class TextureUtils {

	private static HashMap<String, Integer> textures = new HashMap<String, Integer>();
	
	public static void loadTextures(Context context) {
		textures.put("progressbkg",	loadTexture(context, R.drawable.progressbkg));
		textures.put("score", loadTexture(context, R.drawable.score));
		textures.put("blockdisplaybkg", R.drawable.blockdisplaybkg);
		textures.put("bkg", loadTexture(context, R.drawable.bkg));
		
		int[] t = loadTextureSheet(context, R.drawable.sweetblocks, 128, 128);
		for(int i = 0; i < t.length; i++) {
			textures.put("block" + i, t[i]);
		}
		t = loadTextureSheet(context, R.drawable.combos, 128, 128);
		for(int i = 1; i <= t.length; i++) {
			textures.put("combo" + i, t[i-1]);
		}
		t = loadTextureSheet(context, R.drawable.digits, 64, 128);
		for(int i = 0; i < t.length; i++) {
			textures.put("digit" + i, t[i]);
		}
		t = loadTextureSheet(context, R.drawable.superblocks, 128, 128);
		for(int i = 0; i < t.length; i++) {
			textures.put("superblock" + i, t[i]);
		}
		t = loadTextureSheet(context, R.drawable.explosion, 128, 128);
		for(int i = 0; i < t.length; i++) {
			textures.put("explosion" + i, t[i]);
		}
	}
	
	public static int getTexture(String name) {
		if(textures.get(name) == null)
			throw new RuntimeException("getTexture() error: WRONG TEXTURE NAME");
		else return textures.get(name);
	}

	private static int loadTexture(final Context context, final int resID) {
		
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;

		final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resID, options);
		
		if(bitmap == null)
			throw new RuntimeException("loadTexture() error: bitmap could not be loaded!");
		
		final int[] textureHandle = new int[1];
		
		glGenTextures(1, textureHandle, 0);
		
		if(textureHandle[0] != 0) {
			
			glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			
			GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
			
			bitmap.recycle();
			
		} else throw new RuntimeException("Error loading texture. Resource ID: " + resID);
		
		return textureHandle[0];
	}
	
	private static int[] loadTextureSheet(Context context, int resID, int sizeX, int sizeY) {
		
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;

		final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resID, options);
		
		if(bitmap == null)
			throw new RuntimeException("loadTexture() error: bitmap could not be loaded!");
		
		final int xTex = bitmap.getWidth() / sizeX;
		final int yTex = bitmap.getHeight() / sizeY;
		final int texCount = xTex * yTex;
		
		final int[] textureHandle = new int[texCount];
		
		glGenTextures(texCount, textureHandle, 0);
		
		for(int y = 0; y < yTex; y++) {
			for(int x = 0; x < xTex; x++) {
				int i = y * xTex + x;
				if(textureHandle[i] != 0) {
					
					Bitmap tmp = Bitmap.createBitmap(bitmap, x*sizeX, y*sizeY, sizeX, sizeY);
					
					glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[i]);
					
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					
					GLUtils.texImage2D(GL_TEXTURE_2D, 0, tmp, 0);
					
					tmp.recycle();
					
				} else throw new RuntimeException("Error loading texture. Resource ID: " + resID + ". Tex No: " + i );
			}
		}
		
		bitmap.recycle();
		
		return textureHandle;
		
	}
	
}
