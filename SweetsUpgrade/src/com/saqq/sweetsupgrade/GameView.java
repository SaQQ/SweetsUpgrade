package com.saqq.sweetsupgrade;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.NextBlockDisplay;
import com.saqq.sweetsupgrade.objects.ProgressDisplay;
import com.saqq.sweetsupgrade.objects.SaveState;
import com.saqq.sweetsupgrade.objects.ScoreDisplay;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class GameView extends GLSurfaceView implements GameObject {

	/************* OBJECTS **************/
	public Board board;
	public ScoreDisplay scoreDisplay;
	public NextBlockDisplay nbDisplay;
	public ProgressDisplay progressDisplay;
	private GameActivity parentContext;
	
	private GLRect bkg;
	
	public GameView(Context context) {
		super(context);
		parentContext = (GameActivity)context;
		
	}

	public void init() {
		// Here starts execution of game logic. OpenGL is fully loaded.

		float w = getWidth();
		float h = getHeight();
		
		float ratio = h / w;

		final float offset = 0.125f;

		board = new Board(offset, parentContext);
		scoreDisplay = new ScoreDisplay(ratio);
		nbDisplay = new NextBlockDisplay(ratio);
		progressDisplay = new ProgressDisplay(offset);

		board.addObserver(scoreDisplay);
		board.addObserver(nbDisplay);
		board.addObserver(progressDisplay);

		SaveState ss = new SaveState(parentContext);
		
		try {
			if(!ss.loadGame(this))
				board.createBlocks();
		} catch (Exception e) {
			//if somehow loading has failed create new one
			e.printStackTrace();
			board = new Board(offset, parentContext);
			board.createBlocks();
		}
		
		bkg = new GLRect(1.0f, ratio);
		bkg.setPosition(0.0f, 0.0f);
		bkg.setTexture(TextureUtils.getTexture("bkg"));
			
		// now rendering starts
	}

	private float xDown, yDown, xTrans, yTrans;
	private static final float MIN_TRANS = 30.0f;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(board == null) return false;
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			xDown = event.getX();
			yDown = event.getY();
			break;
		case MotionEvent.ACTION_UP:
			xTrans = event.getX() - xDown;
			yTrans = event.getY() - yDown;
			if (xTrans * xTrans + yTrans * yTrans < MIN_TRANS) {
				if (this.getWidth() - event.getX() < 2 * MIN_TRANS) {
					progressDisplay.toggle();
				} else {
					board.tap();
				}
				break;
			}
			if (Math.abs(xTrans) > Math.abs(yTrans)) {
				if (xTrans > 0) {
					board.swipeRight();
				} else {
					board.swipeLeft();
				}
			} else {
				if (yTrans > 0) {
					board.swipeDown();
				} else {
					board.swipeUp();
				}
			}
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	public void update(int t) {
		board.update(t);
		scoreDisplay.update(t);
		nbDisplay.update(t);
		progressDisplay.update(t);
	}

	@Override
	public void draw() {
		bkg.draw();
		board.draw();
		scoreDisplay.draw();
		nbDisplay.draw();
		progressDisplay.draw();
	}
	
	public void onPause() {
		super.onPause();
		board.onPause();
	}

}
