package com.saqq.sweetsupgrade.objects;

import java.util.Observable;
import java.util.Observer;

import com.saqq.sweetsupgrade.GameObject;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class ScoreDisplay implements GameObject, Observer {
	
	private static final int timeForUpdate = 2000;
	
	private static final int MAX_SCORE_DIGITS = 10;
	
	private static final float margin = 0.02f;
	private static float scoreSize;
	
	private GLRect background;

	private float xPos;
	private float yPos;
	
	private int score;
	private int targetScore;
	private int ds;
	
	private int scoreLength;
	private GLRect[] digits;

	public ScoreDisplay(final float ratio) {
		scoreSize = Block.size;
		
		this.xPos = margin;
		this.yPos = ratio - (margin + scoreSize);
		
		background = new GLRect(scoreSize * 2, scoreSize);
		background.setPosition(margin, 0.0f);
		background.setTexture(TextureUtils.getTexture("score"));
		
		score = 0;
		
		digits = new GLRect[MAX_SCORE_DIGITS];
		for(int i = 0; i < MAX_SCORE_DIGITS; i++) {
			digits[i] = new GLRect(scoreSize/2, scoreSize);
			digits[i].setPosition(margin + scoreSize * 2 + i * scoreSize/2, 0.0f);
		}
		
		setScore(0);
		
	}

	@Override
	public void update(Observable observable, Object data) {
		 addScore(((Board)observable).getScore() - this.score);
	}

	@Override
	public void update(int t) {
		if(targetScore > score) {
			score += ds;
			if(score > targetScore) 
				score = targetScore;
			setScore(score);
		}
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();
		
		r.pushMatrix();
		
		r.translate(xPos, yPos, 0.0f);
		
		background.draw();
		
		for(int i = 0; i < scoreLength; i++) {
			digits[i].draw();
		}
		
		r.popMatrix();
	}
	// TODO Auto-generated method stub

	public int getScore() {
		return this.score;
	}
	
	private char[] buffer = new char[MAX_SCORE_DIGITS];;
	
	public void setScore(int score) {
		this.score = score;
		String s = Integer.toString(score);
		scoreLength = s.length();
		s.getChars(0, s.length(), buffer, 0);
		char c;
		for(int i = 0; i < scoreLength; i++) {
			c = buffer[i];
			switch(c) {
			case '0':
				digits[i].setTexture(TextureUtils.getTexture("digit0"));
				break;
			case '1':
				digits[i].setTexture(TextureUtils.getTexture("digit1"));
				break;
			case '2':
				digits[i].setTexture(TextureUtils.getTexture("digit2"));
				break;
			case '3':
				digits[i].setTexture(TextureUtils.getTexture("digit3"));
				break;
			case '4':
				digits[i].setTexture(TextureUtils.getTexture("digit4"));
				break;
			case '5':
				digits[i].setTexture(TextureUtils.getTexture("digit5"));
				break;
			case '6':
				digits[i].setTexture(TextureUtils.getTexture("digit6"));
				break;
			case '7':
				digits[i].setTexture(TextureUtils.getTexture("digit7"));
				break;
			case '8':
				digits[i].setTexture(TextureUtils.getTexture("digit8"));
				break;
			case '9':
				digits[i].setTexture(TextureUtils.getTexture("digit9"));
				break;
			default:
				break;
				
			}
		}
	}
	
	public void addScore(int score) {
		targetScore = this.score + score;
		ds = score / timeForUpdate;
		if(ds < 1) ds = 1;
	}

}
