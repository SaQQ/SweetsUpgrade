package com.saqq.sweetsupgrade.objects;

//import java.util.Random;

import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class Combo extends GLRect {
	
	/* COMBO LIFETIME
	 * 0 - 500ms : appear
	 * 500ms - 1500ms : hold still
	 * 1500ms - 2000ms : disappear
	 */
	
	public static float size;
	
	private static final int lifeTime = 2000;
	
	private int level;
	
	private int timeLeft;
	public boolean isDead;
	
	//private Random random;
	
	private float scale;

	public Combo(int level, float xParent, float yParent) {
		super(size, size);
		
		//random = new Random();
		
		setLevel(level);
		timeLeft = lifeTime;
		isDead = false;
		
		this.x = xParent;
		this.y = yParent;
		
		this.setAlpha(0.0f);
		
		scale = 3.0f;
	}

	@Override
	public void update(int t) {
		timeLeft -= t;
		
		if(timeLeft > 1500) {
			alpha += 1.0f/500 * t;
			scale -= 2.0f/500 * t;
		} else if(timeLeft > 500) {
			
		} else {
			alpha -= 1.0f/500 * t;
		}
		
		if(timeLeft < 0)
			isDead = true;
	}
	
	protected void modifyMatrix(GLRenderer r) {
		r.translate((1-scale)*size/2, (1-scale)*size/2, 0.0f);
		r.scale(scale, scale, 1.0f);
	}
	
	public int getLevel() {
		return this.level;
	}

	public void setLevel(int l) {
		this.level = l;
		setTexture(TextureUtils.getTexture("combo" + l));
	}
}
