package com.saqq.sweetsupgrade.objects.blocks.animations;

import com.saqq.sweetsupgrade.objects.blocks.Block;

public class DropAnimation extends Animation {

	private final static float acceleration = Block.size/20000;

	private float ySpeed;

	private final float yBound;

	public DropAnimation(Block block, int depth) {
		super(block);
		
		ySpeed = 0.0f;

		yBound = Math.abs(depth * Block.size);
	}

	@Override
	public void tick(int t) {
		ySpeed += t * acceleration;
		
		block.yOffset -= ySpeed * t;
		
		if(Math.abs(block.yOffset) >= yBound)
			endAnimation();
	}

}
