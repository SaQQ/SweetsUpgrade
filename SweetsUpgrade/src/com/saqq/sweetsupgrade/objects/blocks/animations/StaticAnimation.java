package com.saqq.sweetsupgrade.objects.blocks.animations;

import com.saqq.sweetsupgrade.objects.blocks.Block;

public class StaticAnimation extends Animation {
	
	private final static float speed = Block.size / 200;
	
	private float xSpeed;
	private float ySpeed;
	
	private final float xBound;
	private final float yBound;

	public StaticAnimation(Block block, int xDist, int yDist) {
		super(block);
		xSpeed = xDist * speed;
		ySpeed = yDist * speed;
		
		xBound = Math.abs(xDist * Block.size);
		yBound = Math.abs(yDist * Block.size);
	}

	@Override
	public void tick(int t) {
		block.xOffset += xSpeed * t;
		block.yOffset += ySpeed * t;
		if(Math.abs(block.xOffset) >= xBound && Math.abs(block.yOffset) >= yBound)
			endAnimation();
	}

}
