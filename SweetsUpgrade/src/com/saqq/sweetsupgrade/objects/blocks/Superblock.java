package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;

public abstract class Superblock extends Block {

	public static final int FLAG = 999;
	
	public static final int BOMB = 0;
	public static final int ROW = 1;
	public static final int ARROW = 2;
	
	public Superblock(Board board, int x, int y) {
		super(board, x, y);
	}
	
}
