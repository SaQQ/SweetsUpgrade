package com.saqq.sweetsupgrade.objects;

import java.util.StringTokenizer;

import android.content.Context;
import android.content.SharedPreferences;

import com.saqq.sweetsupgrade.GameView;
import com.saqq.sweetsupgrade.objects.blocks.ArrowBlock;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.objects.blocks.BombBlock;
import com.saqq.sweetsupgrade.objects.blocks.RowBlock;
import com.saqq.sweetsupgrade.objects.blocks.Superblock;
import com.saqq.sweetsupgrade.objects.blocks.SweetBlock;

public class SaveState {

	private final static boolean GAME_SAVED = true;
	private final static boolean GAME_NOT_SAVED = false;

	private String currString;
	private String boardString;
	private SharedPreferences sharedPref;
	private SharedPreferences.Editor editor;

	public SaveState(Context mainActivity) {
		sharedPref = mainActivity.getSharedPreferences(
				"com.saqq.sweetsupgrade.SAVESTATE_FILE_KEY",
				Context.MODE_PRIVATE);
		editor = sharedPref.edit();
	}
	
	public boolean ifSaved() {
		return sharedPref.getBoolean("savestate", GAME_NOT_SAVED);
	}

	public void saveBoard(Board board) {
		boardString = "";
		currString = "";
		
		editor.putBoolean("savestate", GAME_SAVED);
		
		editor.putInt("score", board.getScore());
		editor.putInt("maxLevel", board.getMaxLeveL());

		editor.putInt(("nextBlock0"), board.nextBlock[0]);
		editor.putInt(("nextBlock1"), board.nextBlock[1]);
		
		if(board.currBlocks[1] == null)
			currString = board.currBlocks[0].xPos + "*" + board.currBlocks[0].yPos + "*"; //superblock case
		else
			currString = board.currBlocks[0].xPos + "*" + board.currBlocks[0].yPos + "*" + board.currBlocks[1].xPos + "*" + board.currBlocks[1].yPos + "*";

		editor.putString("currBlock", currString);

		for (int y = 0; y < Board.yTiles; y++) {
			for (int x = 0; x < Board.xTiles; x++) {
				boardString += getBlock(board.blocks[x][y]) + "*";
			}
		}
		
		editor.putString("board", boardString);
		
		editor.putBoolean("superblock", board.superblock);
		
		editor.putInt("rotState", board.rotState);

		editor.commit();
	}

	public Block[][] loadBoard(Board board) {

		Block[][] table = new Block[Board.xTiles][Board.yTiles];

		String boardString;

		boardString = sharedPref.getString("board", "0");

		if (boardString == "0")
			return table;

		StringTokenizer st = new StringTokenizer(boardString, "*");
		
		for (int y = 0; y < Board.yTiles; y++) {
			for (int x = 0; x < Board.xTiles; x++) {
				if(st.hasMoreTokens())
					table[x][y] = createBlock(st.nextToken(), board, x, y);
			}
		}

		return table;
	}

	private Block[] loadCurrBlock(Board board) {

		Block[] table = new Block[2];

		String currString;

		currString = sharedPref.getString("currBlock", "0");

		if (currString == "0") {
			table[0] = board.blocks[3][8];
			table[1] = board.blocks[4][8];
			return table;
		}
		
		StringTokenizer st = new StringTokenizer(currString, "*");
		
		table[0] = board.blocks[Integer.parseInt(st.nextToken())][Integer.parseInt(st.nextToken())];
		if(st.hasMoreTokens()) //superblock case
			table[1] = board.blocks[Integer.parseInt(st.nextToken())][Integer.parseInt(st.nextToken())];

		return table;

	}

	private String getBlock(Block block) {
		if(block == null) {
			return "x";
		} else if (block instanceof SweetBlock) {
			return Integer.toString(((SweetBlock) block).getLevel());
		} else if (block instanceof Superblock) {
			if (block instanceof BombBlock)
				return "b";
			else if (block instanceof RowBlock)
				return "r";
			else if (block instanceof ArrowBlock)
				return "a";
		}
		return null;
	}

	private Block createBlock(String symbol, Board board, int x, int y) {
		if(symbol.equals("x"))
			return null;
		else if (symbol.equals("b"))
			return new BombBlock(board, x, y);
		else if (symbol.equals("r"))
			return new RowBlock(board, x, y);
		else if (symbol.equals("a"))
			return new ArrowBlock(board, x, y);
		else if (Integer.parseInt(symbol) >= 0)
			return new SweetBlock(board, x, y, Integer.parseInt(symbol));
		else return null;
	}

	public void saveGame(GameView gv) {
		saveBoard(gv.board);
	}

	public boolean loadGame(GameView gv) throws Exception {

		if (sharedPref.getBoolean("savestate", GAME_NOT_SAVED) == GAME_NOT_SAVED)
			return false;

		gv.board.setScore(sharedPref.getInt("score", 0));
		gv.scoreDisplay.setScore(sharedPref.getInt("score", 0));

		gv.board.setMaxLevel(sharedPref.getInt("maxLevel", 0));
		gv.progressDisplay.setMaxLevel(sharedPref.getInt("maxLevel", 0));

		gv.board.blocks = loadBoard(gv.board);
		gv.board.currBlocks = loadCurrBlock(gv.board);
		gv.board.superblock = sharedPref.getBoolean("superblock", false);
		
		gv.board.nextBlock[0] = sharedPref.getInt(("nextBlock0"), 0);
		gv.board.nextBlock[1] = sharedPref.getInt(("nextBlock1"), 0);

		gv.nbDisplay.setBlocks(gv.board.nextBlock[0], gv.board.nextBlock[1]);

		gv.board.rotState = sharedPref.getInt("rotState", 0);
		
		

		return true;
	}
	
	public void deleteSave() {
		editor.putBoolean("savestate", GAME_NOT_SAVED);
		editor.commit();
	}

}
