package com.saqq.sweetsupgrade.objects;

import java.util.Observable;
import java.util.Observer;

import com.saqq.sweetsupgrade.GameObject;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.objects.blocks.Superblock;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class NextBlockDisplay implements GameObject, Observer {
	
	private static float blockSize;
	private static float padding = 0.02f;
	private static float margin = 0.02f;
	
	private GLRect background;
	private GLRect[] blocks;
	
	private float xPos;
	private float yPos;
			
	public NextBlockDisplay(final float ratio) {
		blockSize = Block.size;
		blocks = new GLRect[2];
		blocks[0] = new GLRect(blockSize, blockSize);
		blocks[1] = new GLRect(blockSize, blockSize);
		blocks[0].setPosition(padding, padding);
		blocks[1].setPosition(2 * padding + blockSize, padding);
		background = new GLRect(blockSize * 2 + padding * 3, blockSize + 2 * padding);
		background.setPosition(0.0f, 0.0f);
		background.setTexture(TextureUtils.getTexture("blockdisplaybkg"));
		background.setAlpha(0.8f);
		
		this.xPos = 1.0f - (margin + 2 * blockSize + 3 * padding);
		this.yPos = ratio - (margin + blockSize + 2 * padding);
	}

	@Override
	public void update(Observable observable, Object data) {
		setBlocks(((Board)observable).nextBlock[0], ((Board)observable).nextBlock[1]);
	}

	@Override
	public void update(int t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();
		
		r.pushMatrix();
		
		r.translate(xPos, yPos, 0.0f);
		
		background.draw();
		blocks[0].draw();
		blocks[1].draw();
		
		r.popMatrix();
		
	}
	
	public void setBlocks(int l1, int l2) {
		if(l1 == Superblock.FLAG) {
			blocks[0].setTexture(0);
			blocks[1].setTexture(TextureUtils.getTexture("superblock" + l2));
		} else {
			blocks[0].setTexture(TextureUtils.getTexture("block" + l1));
			blocks[1].setTexture(TextureUtils.getTexture("block" + l2));
		}
	}

}
