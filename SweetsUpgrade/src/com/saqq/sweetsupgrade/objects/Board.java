package com.saqq.sweetsupgrade.objects;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

import com.saqq.sweetsupgrade.GameActivity;
import com.saqq.sweetsupgrade.GameObject;
import com.saqq.sweetsupgrade.objects.blocks.ArrowBlock;
import com.saqq.sweetsupgrade.objects.blocks.BackgroundBlock;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.objects.blocks.BombBlock;
import com.saqq.sweetsupgrade.objects.blocks.RowBlock;
import com.saqq.sweetsupgrade.objects.blocks.Superblock;
import com.saqq.sweetsupgrade.objects.blocks.SweetBlock;
import com.saqq.sweetsupgrade.objects.effects.Effect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;

public class Board extends Observable implements GameObject {

	public static final int xTiles = 8;
	public static final int yTiles = 10;

	private float xPos;
	private float yPos;

	private boolean bMoving;
	private ArrayList<Block> moving;
	private boolean bDropping;

	private Random random;

	private ArrayList<ArrayList<Block>> collapseNodes;
	private boolean[][] collapseMark;

	/************* OBJECTS **************/
	public Block[][] blocks;
	public BackgroundBlock[][] background;
	// Always store the very left one in currBlocks[0],
	// and the very right one in currBlocks[1];
	public Block[] currBlocks = new Block[2];

	private ArrayList<Combo> combos;
	private int currCombo;
	private int countScore;
	
	private ArrayList<Effect> effects;

	private int currMaxLevel;

	public int[] nextBlock = new int[2];
	public boolean superblock;
	
	private int score;
	
	private transient Highscore highscore;
	
	private transient GameActivity parentContext;

	public Board(final float yOffset, GameActivity context) {
		xPos = 0.0f;
		yPos = yOffset; // just temp ;p

		Block.size = 1.0f / xTiles;
		blocks = new Block[xTiles][yTiles];
		background = new BackgroundBlock[xTiles][yTiles];

		boolean starting;
		for (int x = 0; x < xTiles; x++) {
			for (int y = 0; y < yTiles; y++) {
				if (y >= yTiles - 3)
					starting = true;
				else
					starting = false;
				background[x][y] = new BackgroundBlock(this, x, y, starting);
			}
		}
		
	
		parentContext = context;

		bMoving = false;
		moving = new ArrayList<Block>();
		bDropping = false;

		random = new Random();
		
		collapseNodes = new ArrayList<ArrayList<Block>>();
		collapseMark = new boolean[xTiles][yTiles];

		currMaxLevel = 0;

		combos = new ArrayList<Combo>();
		currCombo = 0;
		
		effects = new ArrayList<Effect>();

		Combo.size = Block.size;
		
		score = 0;
		highscore = new Highscore(parentContext);
	}

	@Override
	public void update(int t) {
		
		if(forcingEnd) {
			for(int i = 0; i < moving.size(); i++) {
				moving.get(i).forceMoveEnd();
			}
			for(int i = 0; i < effects.size(); i++ ) {
				effects.get(i).forceAnimationEnd();
			}
		}
		
		for (int x = 0; x < xTiles; x++) {
			for (int y = 0; y < yTiles; y++) {
				background[x][y].update(t);
			}
		}

		for (int i = 0; i < moving.size(); i++) {
			moving.get(i).update(t);
			if (moving.get(i).moveFinished) {
				moving.remove(i--);
				if (moving.size() == 0) // this is ugly... uhhh...
					if (bDropping)
						dropStep();
					else
						endMove();
			}
		}
		
		for(int i = 0; i < effects.size(); i++) {
			effects.get(i).update(t);
			if(effects.get(i).isDead)
				effects.remove(i--);
		}

		for (int i = 0; i < combos.size(); i++) {
			combos.get(i).update(t);
			if (combos.get(i).isDead)
				combos.remove(i--);
		}
	
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();

		r.pushMatrix();

		r.translate(xPos, yPos, 0.0f);

		for (int y = 0; y < yTiles; y++) {
			for (int x = 0; x < xTiles; x++) {
				background[x][y].draw();
			}
		}

		for (int y = 0; y < yTiles; y++) {
			for (int x = 0; x < xTiles; x++) {
				Block b = blocks[x][y];
				if (b != null)
					b.draw();
			}
		}
		
		for(int i = 0; i < effects.size(); i++) {
			effects.get(i).draw();
		}

		for (int i = 0; i < combos.size(); i++) {
			combos.get(i).draw();
		}

		r.popMatrix();
	}
	
	public void printBoard() {
		System.out.println("/////////BOARD://///////");
		for(int y = yTiles - 1; y >= 0; y--) {
			for(int x = 0; x < xTiles; x++) {
				Block b = blocks[x][y];
				if(b instanceof SweetBlock)
					System.out.print("|" + ((SweetBlock)b).getLevel() + "|");
				else
					System.out.print("| |");
			}
			System.out.print("\n");
		}
		System.out.println("////////////////////////");
	}

	private boolean checkAndDrop() {
		boolean canDrop = false;
		
		boolean[][] mute = new boolean[xTiles][yTiles];

		boolean rep;
		
		for (int y = 0; y < yTiles; y++) {
			for (int x = 0; x < xTiles; x++) {
				if(blocks[x][y] != null) {
					blocks[x][y].dropDist = 0;
					blocks[x][y].baseX = x;
					blocks[x][y].baseY = y;
				}
				mute[x][y] = false;
			}
		}
		
		do {
			
			rep = false;
		
			for (int y = 1; y < yTiles; y++) {
				for (int x = 0; x < xTiles; x++) {
					if (blocks[x][y] != null && blocks[x][y - 1] == null && !mute[x][y]) {

						int i = y;
						while (i < yTiles && blocks[x][i] != null && !mute[x][i]) {
							blocks[x][i].dropDist++;
							blocks[x][i-1] = blocks[x][i];
							blocks[x][i] = null;
							i++;
						}
						rep = true;
					}
				}
			}
			
			for (int y = 0; y < yTiles; y++) {
				for (int x = 0; x < xTiles; x++) {
					this.clearMarks();
					if(blocks[x][y] instanceof SweetBlock)
						if(collapseFunc(x, y, ((SweetBlock) blocks[x][y]).getLevel(), null) > 2) {
							mute[x][y] = true;
						}
						
				}
			}

		} while(rep);
		
		for (int y = yTiles - 1; y >= 0; y--) {
			for (int x = 0; x < xTiles; x++) {
				Block b = blocks[x][y];
				if(b == null) 
					continue;
				if(x != b.baseX || y != b.baseY) {
					blocks[x][y] = null;
					blocks[b.baseX][b.baseY] = b;
				}
			}
		}
		
		for (int y = 0; y < yTiles; y++) {
			for (int x = 0; x < xTiles; x++) {
				if(blocks[x][y] != null && blocks[x][y].dropDist != 0 ) {
					blocks[x][y].drop(blocks[x][y].dropDist);
					moving.add(blocks[x][y]);
					canDrop = true;
				}
			}
		}

		return canDrop;
	}

	private boolean checkAndCollapse() {

		this.clearMarks();
		
		// resolve previous collapses//
		for (int i = 0; i < collapseNodes.size(); i++) {
			updateMaxLevel(((SweetBlock) blocks[collapseNodes.get(i).get(0).xPos][collapseNodes
					.get(i).get(0).yPos]).incrementLevel());
		}
		// **************************//

		collapseNodes.clear();

		boolean canCollapse = false;
		ArrayList<Block> tmp;
		for (int y = 0; y < yTiles; y++) {
			for (int x = 0; x < xTiles; x++) {
				Block b = blocks[x][y];
				if (b instanceof SweetBlock) {
					tmp = new ArrayList<Block>();
					int surr = collapseFunc(x, y, ((SweetBlock) b).getLevel(),
							tmp);
					if (surr > 2) {
						canCollapse = true;
						collapseNodes.add(tmp);
					}
				}
			}
		}

		Block base;
		Block curr;

		for (int i = 0; i < collapseNodes.size(); i++) {
			ArrayList<Block> list = collapseNodes.get(i);

			base = list.get(0);
			
			currCombo++;
			
			countScore += (int)(Math.pow((((SweetBlock)base).getLevel()+1),2) * list.size());
			
			if (currCombo > 1)
				combos.add(new Combo(currCombo, base.getX(), base.getY()));

			for (int j = 1; j < list.size(); j++) {
				curr = list.get(j);
				curr.move(base.xPos - curr.xPos, base.yPos - curr.yPos);
				moving.add(curr);
			}
		}
		return canCollapse;
	}

	private int collapseFunc(int x, int y, int level, ArrayList<Block> tmp) {
		int i = 0;
		if (x >= 0 && y >= 0 && x < xTiles && y < yTiles
				&& collapseMark[x][y] == false
				&& blocks[x][y] instanceof SweetBlock
				&& ((SweetBlock) blocks[x][y]).getLevel() == level) {
			i++;
			collapseMark[x][y] = true;
			if(tmp != null) tmp.add(blocks[x][y]);
			i += collapseFunc(x, y - 1, level, tmp);
			i += collapseFunc(x + 1, y, level, tmp);
			i += collapseFunc(x, y + 1, level, tmp);
			i += collapseFunc(x - 1, y, level, tmp);
		}
		return i;

	}
	
	private void clearMarks() {
		for (int x = 0; x < xTiles; x++) {
			for (int y = 0; y < yTiles; y++) {
				collapseMark[x][y] = false;
			}
		}
	}

	public void dropStep() {
		if (checkAndCollapse())
			return;
		else if (checkAndDrop())
			return;
		else {
			endMove();
			if (checkLoss())
				endGame();
			createBlocks();
		}
	}

	private void endMove() {
		bMoving = false;
		bDropping = false;
		addScore(countScore * currCombo);
		countScore = 0;
		currCombo = 0;
		forcingEnd = false;
	}

	public void swipeDown() {
		if (bMoving)
			return;
		if(currBlocks[0] instanceof SweetBlock)
			addScore((int)Math.pow((((SweetBlock)(currBlocks[0])).getLevel()+1), 2) + (int)Math.pow((((SweetBlock)(currBlocks[1])).getLevel()+1), 2));
		bMoving = true;
		bDropping = true;
		dropStep();
	}

	public void swipeUp() {
		
	}

	public void swipeLeft() {
		if (bMoving)
			return;
		if (currBlocks[0].xPos == 0)
			return;
		bMoving = true;
		currBlocks[0].move(-1, 0);
		if(!superblock)
			currBlocks[1].move(-1, 0);
		moving.add(currBlocks[0]);
		if(!superblock)
			moving.add(currBlocks[1]);
	}

	public void swipeRight() {
		if (bMoving)
			return;
		if(superblock) {
			if(currBlocks[0].xPos == xTiles - 1)
				return;
		} else {
			if(currBlocks[1].xPos == xTiles - 1)
				return;
		}
		bMoving = true;
		if(!superblock)
			currBlocks[1].move(1, 0);
		currBlocks[0].move(1, 0);
		if(!superblock)
			moving.add(currBlocks[1]);
		moving.add(currBlocks[0]);
	}

	public int rotState;

	public void tap() {
		if (bMoving)
			return;
		if(superblock)
			return;
		bMoving = true;
		switch (rotState) {
		case 0:
			currBlocks[0].move(0, 1);
			currBlocks[1].move(-1, 0);
			moving.add(currBlocks[0]);
			moving.add(currBlocks[1]);
			rotState = 1;
			break;
		case 1:
			if (currBlocks[0].xPos == xTiles - 1) {
				currBlocks[0].move(0, -1);
				currBlocks[1].move(-1, 0);
				moving.add(currBlocks[1]);
				moving.add(currBlocks[0]);
			} else {
				currBlocks[0].move(1, -1);
				moving.add(currBlocks[0]);
			}
			rotState = 0;

			// swap them
			Block tmp = currBlocks[0];
			currBlocks[0] = currBlocks[1];
			currBlocks[1] = tmp;
			break;
		}
	}

	private float chance;
	
	public void createBlocks() {
		if(nextBlock[0] == Superblock.FLAG) {
			superblock = true;
			switch(nextBlock[1]) {
			case Superblock.BOMB:
				blocks[3][8] = currBlocks[0] = new BombBlock(this, 3, 8);
				blocks[4][8] = currBlocks[1] = null;
				break;
			case Superblock.ROW:
				blocks[3][8] = currBlocks[0] = new RowBlock(this, 3, 8);
				blocks[4][8] = currBlocks[1] = null;
				break;
			case Superblock.ARROW:
				blocks[3][8] = currBlocks[0] = new ArrowBlock(this, 3, 8);
				blocks[4][8] = currBlocks[1] = null;
				break;
			default:
				break;
			}
		} else {
			superblock = false;
			blocks[3][8] = currBlocks[0] = new SweetBlock(this, 3, 8, nextBlock[0]);
			blocks[4][8] = currBlocks[1] = new SweetBlock(this, 4, 8, nextBlock[1]);
		}
		
		rotState = 0;
		
		chance =  random.nextFloat();
		if(chance > 0.97f) {
			nextBlock[0] = Superblock.FLAG;
			chance = random.nextFloat();
			
			int c = ((int)(chance * 100)) % 3; //equal chance for each block
			
			nextBlock[1] = c;
		} else {
			nextBlock[0] = Math.abs(random.nextInt() % (currMaxLevel + 1));
			nextBlock[1] = Math.abs(random.nextInt() % (currMaxLevel + 1));
		}

		this.setChanged();
		this.notifyObservers();
	}

	public int getMaxLeveL() {
		return this.currMaxLevel;
	}
	
	public void setMaxLevel(int currMaxLevel) {
		this.currMaxLevel = currMaxLevel;
	}

	public void updateMaxLevel(int neo) {
		if (neo > currMaxLevel) {
			currMaxLevel = neo;
			this.setChanged();
			this.notifyObservers();
		}
	}

	public boolean checkLoss() {
		boolean loose = false;
		for (int i = 0; i < xTiles; i++) {
			if (blocks[i][yTiles - 3] != null) {
				loose = true;
				break;
			}
		}
		return loose;
	}
	
	private boolean forcingEnd = false;
	
	public void forceMoveEnd() {
		forcingEnd = true;
		while(!moving.isEmpty()) {
			update(0);
		}
	}
	
	public int getScore() {
		return this.score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public void addScore(int score)	 {
		this.score += score;
		this.setChanged();
		this.notifyObservers();
	}
	
	public void addEffect(Effect e) {
		effects.add(e);
	}
	
	public boolean destroyBlock(int x, int y) {
		if(x >= 0 && y >= 0 && x < xTiles && y < yTiles) {
			blocks[x][y] = null;
			return true;
		} else return false;
	}
	
	public boolean gameFinished = false;

	public void endGame() {
		//TODO
		
		parentContext.showFinishDialog(score);
		
		highscore.addScore(score);
		
		gameFinished = true;

		//parentContext.finish();
		
	}
	
	public void onPause() {
		this.forceMoveEnd();
		SaveState ss = new SaveState(parentContext);
		ss.saveGame(parentContext.getView());
		if(gameFinished) {
			ss.deleteSave();
			gameFinished = false;
		}
	}

}
