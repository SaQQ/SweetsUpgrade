package com.saqq.sweetsupgrade.objects;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Highscore {

	public Highscore (Activity mainActivity) {
		
		defScore = "";
		for(int i = 0; i < DEFAULT_HIGHSCORE_SIZE; i++)
			defScore += "0*";
		
		sharedPref = mainActivity.getSharedPreferences("com.saqq.sweetsupgrade.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
		high_score = sharedPref.getString("saved_score",defScore);
		editor = sharedPref.edit();
	}
	
	public void addScore(int score)
	{	
		int firstIndex=0, lastIndex=-1, value;
		boolean ifHigher = false;
		while(high_score.indexOf('*',lastIndex+1) != -1 && ifHigher == false)
		{
			firstIndex = lastIndex+1;
			lastIndex = high_score.indexOf('*', lastIndex+1); 
			value = Integer.parseInt(high_score.substring(firstIndex,lastIndex));
			if(score >= value)
			{
				high_score = high_score.substring(0,firstIndex)  + Integer.toString(score) + "*" + high_score.substring(firstIndex);
				ifHigher = true;
				high_score = high_score.substring(0,high_score.lastIndexOf('*',high_score.length()-2)+1);
			}			
		}
		//System.out.print(high_score + "\n");
		editor.putString("saved_score", high_score);
		editor.commit();
	}
	
	public String[] getScore(){
		String [] highscoreTable = new String[10];
		int firstIndex=0, lastIndex=-1;
		for(int i = 0; i < DEFAULT_HIGHSCORE_SIZE; i++)
		{
			firstIndex = lastIndex+1;
			lastIndex = high_score.indexOf('*', lastIndex+1); 
			highscoreTable[i] = high_score.substring(firstIndex,lastIndex);	
		}
		return highscoreTable;
	}
	public static final int DEFAULT_HIGHSCORE_SIZE = 10;
	private String defScore;
	private String high_score;
	private SharedPreferences sharedPref;
	private SharedPreferences.Editor editor;
}
