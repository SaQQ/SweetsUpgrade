package com.saqq.sweetsupgrade.objects.effects;

import java.util.ArrayList;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.objects.blocks.BombBlock;
import com.saqq.sweetsupgrade.objects.blocks.SweetBlock;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class BombEffect extends Effect {

	private static final int explosionFrames = 48 - 1; // ( -1 cause indices
														// start at 0)
	private static final int animationTime = 1500;
	private static final int frameTime = animationTime / explosionFrames;

	private BombBlock bomb;
	
	private int xPos;
	private int yPos;
	
	private int level;

	private ArrayList<GLRect> graphics;
	private ArrayList<Pair> deleteList;

	private int time;
	private int frame;

	public BombEffect(Board board, BombBlock bomb) {
		super(board);
		
		this.bomb = bomb;
		
		this.xPos = bomb.xPos;
		if(bomb.yPos == 0) {
			onAnimationEnd();
			return;
		}
		this.yPos = bomb.yPos - 1;
		
		if(board.blocks[xPos][yPos] instanceof SweetBlock)
			this.level = ((SweetBlock)board.blocks[xPos][yPos]).getLevel();
		else {
			onAnimationEnd();
			return;
		}
		
		frame = 0;
		time = 0;
		
		graphics = new ArrayList<GLRect>();
		GLRect tmp;
		
		deleteList = new ArrayList<Pair>();
		Pair p;
		
		for(int y = 0; y < Board.yTiles; y++) {
			for(int x = 0; x < Board.xTiles; x++) {
				if(board.blocks[x][y] instanceof SweetBlock) {
					if(((SweetBlock)board.blocks[x][y]).getLevel() == this.level) {
						tmp = new GLRect(Block.size, Block.size);
						tmp.setPosition(x * Block.size, y * Block.size);
						tmp.setTexture(TextureUtils.getTexture("explosion" + frame));
						graphics.add(tmp);
						p = new Pair(x, y);
						deleteList.add(p);
					}
				}
			}
		}
	
	}

	@Override
	public void update(int t) {
		time += t;
		while(time > frameTime) {
			time -= frameTime;
			nextFrame();
			if (frame == explosionFrames / 2)
				deleteBlocks();
		}
		if (frame > explosionFrames)
			onAnimationEnd();
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();

		r.pushMatrix();

		for(int i = 0; i < graphics.size(); i++) {
			graphics.get(i).draw();
		}

		r.popMatrix();

	}
	
	private void nextFrame() {
		for(int i = 0; i < graphics.size(); i++) {
			graphics.get(i).setTexture(TextureUtils.getTexture("explosion" + frame));
		}
		frame++;
	}
	
	public void onAnimationEnd() {
		board.destroyBlock(xPos, yPos + 1);
		this.isDead = true;
		bomb.moveFinished = true;
	}

	public void deleteBlocks() {
		Pair p;
		for(int i = 0; i < deleteList.size(); i++) {
			p = deleteList.get(i);
			board.destroyBlock(p.x, p.y);
		}
	}
	
	public void forceAnimationEnd() {
		deleteBlocks();
		onAnimationEnd();
	}
	
	class Pair {
		public int x;
		public int y;
		
		public Pair(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

}
