package com.saqq.sweetsupgrade.objects.effects;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.blocks.ArrowBlock;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class ArrowEffect extends Effect {

	private static final int explosionFrames = 48 - 1; // ( -1 cause indices
														// start at 0)
	private static final int animationTime = 1500;
	private static final int frameTime = animationTime / explosionFrames;

	private ArrowBlock arrow;
	
	private int xPos;
	private int yPos;

	private GLRect[] graphics;

	private int time;
	private int frame;

	public ArrowEffect(Board board, ArrowBlock arrow) {
		super(board);
		
		this.arrow = arrow;
		
		this.xPos = arrow.xPos;
		this.yPos = arrow.yPos;

		graphics = new GLRect[yPos];
		
		for(int i = 0; i < graphics.length; i++) {
				graphics[i] = new GLRect(Block.size, Block.size);
				graphics[i].setPosition(0.0f, i * Block.size);
				graphics[i].setTexture(TextureUtils.getTexture("explosion" + frame));
		}

		frame = 0;
		time = 0;
	
	}

	@Override
	public void update(int t) {
		time += t;
		while(time > frameTime) {
			time -= frameTime;
			nextFrame();
			if (frame == explosionFrames / 2)
				deleteBlocks();
		}
		if (frame > explosionFrames)
			onAnimationEnd();
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();

		r.pushMatrix();

		r.translate(xPos * Block.size, 0.0f, 0.0f);

		for(int i = 0; i < graphics.length; i++)
			graphics[i].draw();

		r.popMatrix();

	}
	
	private void nextFrame() {
		for(int i = 0; i < graphics.length; i++)
			graphics[i].setTexture(TextureUtils.getTexture("explosion" + frame));
		frame++;
	}
	
	public void onAnimationEnd() {
		this.isDead = true;
		board.destroyBlock(xPos, yPos);
		arrow.moveFinished = true;
	}

	public void deleteBlocks() {
		for(int i = 0; i < yPos; i++)
			board.destroyBlock(xPos, i);
	}
	
	public void forceAnimationEnd() {
		deleteBlocks();
		onAnimationEnd();
	}

}
