package com.saqq.sweetsupgrade.objects.effects;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.objects.blocks.RowBlock;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class RowEffect extends Effect {

	private static final int explosionFrames = 48 - 1; // ( -1 cause indices
														// start at 0)
	private static final int animationTime = 1500;
	private static final int frameTime = animationTime / explosionFrames;

	private RowBlock row;
	
	//private int xPos;
	private int yPos;

	private GLRect[] graphics;

	private int time;
	private int frame;

	public RowEffect(Board board, RowBlock row) {
		super(board);
		
		this.row = row;
		
		//this.xPos = row.xPos;
		this.yPos = row.yPos;

		graphics = new GLRect[Board.yTiles];
		
		for(int i = 0; i < graphics.length; i++) {
				graphics[i] = new GLRect(Block.size, Block.size);
				graphics[i].setPosition(i * Block.size, 0.0f);
				graphics[i].setTexture(TextureUtils.getTexture("explosion" + frame));
		}

		frame = 0;
		time = 0;
	
	}

	@Override
	public void update(int t) {
		time += t;
		while(time > frameTime) {
			time -= frameTime;
			nextFrame();
			if (frame == explosionFrames / 2)
				deleteBlocks();
		}
		if (frame > explosionFrames)
			onAnimationEnd();
	}

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();

		r.pushMatrix();

		r.translate(0.0f, yPos * Block.size, 0.0f);

		for(int i = 0; i < graphics.length; i++)
			graphics[i].draw();

		r.popMatrix();

	}
	
	private void nextFrame() {
		for(int i = 0; i < graphics.length; i++)
			graphics[i].setTexture(TextureUtils.getTexture("explosion" + frame));
		frame++;
	}
	
	public void onAnimationEnd() {
		this.isDead = true;
		row.moveFinished = true;
	}

	public void deleteBlocks() {
		for(int i = 0; i < Board.xTiles; i++)
			board.destroyBlock(i, yPos);
	}
	
	public void forceAnimationEnd() {
		deleteBlocks();
		onAnimationEnd();
	}

}
